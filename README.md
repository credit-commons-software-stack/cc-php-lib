
# Credit Commons PHP library

This php library implements all the API functions to help accelerate development of credit commons apps and to provide a compatibility layer.

it is required by any PHP application which wants to read or write to a Credit commons node via REST. That includes [Reference Node](https://gitlab.com/credit-commons/cc-node) while addressing remote nodes and also the [Developers' client](https://gitlab.com/credit-commons/cc-dev-client) which provides a Full UI to a credit commons node at any URL via REST.

This repo also hosts the [OpenAPI specification](https://gitlab.com/credit-commons/cc-php-lib/-/blob/master/docs/credit-commons-openapi3.yml).

## Installation
This is not a standalone app or a REST server, and is already included as needed in the other credit commons reference implementation repositories.
If you are making a client side application or a new node implementation, first add this repository to your composer.json

    "repositories": [
       {
           "type": "gitlab",
            "url": "git@gitlab.com:credit-commons/cc-php-lib.git"
       }
    ]

Then require it at the command line thus:

`$ composer require credit-commons/cc-php-lib`

##How to connect your exising ledger to a credit commons trunkwards node.
This can be quite difficult depending on how your existing ledger architecture maps onto the credit commons ledger architecture.
### Setup
Ensure Composer includes this repo.
You need to retrieve at least once, the available workflows from the parent node. All remote transactions must use one of these workflow ids. 
GET trunkwardnode.net/workflows
Settings/variables are:
 - The workflow id(s) for transactions you generate
 - The exchange rate (float) The leafward node is responsible for multiplying the transaction quant when sending trunkwards, and dividing the quant received from the trunkwards node.
 - The url of the parent node, and the name of your node in the credit commons tree
 - One account must be designated as the import/export account, and its ID noted.
Storage needed:
 - a hashchain containing the the uuid, the version number, the transaction state if you like, optionally (for debugging) string which is hashed as well.
 - each entry might have remote metadata. Most usefully the path/name of the remote account. (not strictly necessary)
 - each remote transaction must have a UUID, a state.
If you know that there are no transaction fees charged in your tree, you can design your storage assuming that each transaction has one and only one entry.
You'll need a new RemoteTransaction class which uses CreditCommons\TransversalTransactionTrait, and which can be instantiated from a local transaction object AND from an incoming transaction.

### Sending a transaction
Workflows have a direction, which is bill (request units from the remote account) or credit (pay units to the remote account).
Prepare a transaction form in your application. When the form is submitted, prepare an object like this

	<?php
		$properties = [
			'type' => 'legacy_credit', // ID of the workflow
			'payee' => 'path/to/payee', // Local account IDs don't have a path, just the account ID.
			'payer' => 'path/to/payer',
			'description' => 'blah blah',
			'quant' => 100 * $rate
			'metadata' => ['foo' => 'bar'], // Any key/value pair you like
		];
		$transaction = new CreditCommons\NewTransaction($properties);
		$requester = new CreditCommons\NodeRequester($trunkwards_url, $yourNodeName, $account->getLastHash());

Then POST this NewTransaction object to https://trunkwardnode.net/transaction

	<?php
		$guzzle = new \GuzzleHttp\Client(
		['base_uri' => 'https://trunkwardnode.net', 'timeout' => 4] // ces can be slow
	);
	$options = [
		\GuzzleHttp\RequestOptions::BODY => json_encode($transaction),
	];
	$response = $guzzle->post('transaction', $options);

A successful response will return 200 or 201, depending on the workflow->create->confirm, and the full transaction object in the response body 'data'. The transaction may contain more than one entry if other nodes have added fees. It will also have a list of actions the user can take, which is to say states the user can transition it to.
- 201 means the transaction is now in the workflow->creation->state
- 200 means the transaction is merely validated and the user must agree to change its state. You must display a confirmation page.

When the user has confirmed, send the following:

	<?php
		// The uuid is in the full transaction which was previously returned. the state must be one of the transaction action keys.
		$guzzle->patch('transaction/{uuid}/{dest_state}'); 

Now the response should be 201, and you should save the transaction to your local database, 

### Error handling
In case of error you will receive.
- 400 (client error - the fault of the user or the programmer)
- 500 (server error - the fault of the software on the trunkward node.

 Error responses contain a json object which \CreditCommons\NodeRequester converts into one of the objects specified in src/Exceptions. On that object call $err->makeMessage() to generate a string for the user. Note that this will return a string in English.

### Writing a transaction
The local transaction must be saved with the local unit of account.
Convert the remote account address in the payer/payee field to the local import/export account ID, and store the remote path with the transaction. You may need it when you display the transaction. 

### Remotely created transactions
Make a route for POST /transaction/relay
Requests to this route contain a transaction object which should be upcast to your RemoteTransaction class.
That means for each entry the quant must be divided by your exchange rate, and the paths converted to your local accounts IDS, with the remote account always set to your balance of trade account and its path stored elsewhere.
Add any transaction fees by appending a new \CreditCommons\Entry.
Validate the transaction by testing that the local account ID (one for each entry) exists and that the account limit would not be violated. 
Generate an error object by passing the right parameters e.g.

	<?php 
		$err = new MinLimitViolation(diff: 93, account: bob123);
		// You can set up your system so that just throwing this returns the correct response
		throw $err;
If the transaction is valid, check the workflow to see whether to save it in validated state and return 200, or the creation->start state and return 201. Generate the hash, and write the transaction and hash.

	<?php
		$hash = $transaction->makeHash(); //from the TransversalTransactionTrait

### Remotely updated transactions
Make an route for PATCH transaction/{uuid}/{state}.
When the request arrives, load the transaction, optionally check if the remote user is permitted to make this workflow state transition, change the transaction state, increment the version number, generate the hash, save the transaction locally, and return 201.

### Authentication
Every request to the trunkward node is authenticated using the cc-user and cc-auth headers.

	<?php
	\GuzzleHttp\RequestOptions::HEADERS => [
	   'cc-user' => $mynode,
	   'cc-auth' => $last_hash,
	   'accept' => 'application/json'
	];
Incoming requests should always have the cc-user set to the name of the trunkward node which should always be the same so need not be checked, and the hash, which must be the same as the last hash you have stored. If the hash is wrong then throw a \Exceptions\HashMismatchFailure.
