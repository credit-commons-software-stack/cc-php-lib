<?php

namespace CreditCommons;

/**
 * This object represents an account in the ledger. It does not form part of the
 * public API but is strongly implied to be included in the API library.
 *
 * @todo make a new interface to distinguish passive user objects.
 */
abstract class BaseAccount {

  function __construct(
    public string $id
  ) {}

  /**
   * Flatten the object
   */
  function __toString() {
    return $this->id;
  }

}
