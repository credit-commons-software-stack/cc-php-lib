<?php

namespace CreditCommons;
use CreditCommons\Transaction;
use CreditCommons\Entry;
use CreditCommons\Exceptions\InvalidTransitionViolation;
use CreditCommons\Exceptions\WorkflowViolation;

/**
 * Calculate what actions an account can perform on a transaction.
 *
 * @author matslats
 *
 * @todo need to think through how an admin on a trunkward node could transition
 * a transaction and have it propagate to both branches. The protocol needs to
 * allow that any transition coming from a parent node on a transversal
 * transaction must be executed. It's not about being 'admin' or not. The
 * trunkward node MUST log which user initiated it.
 */
class Workflow {

  public $id;
  public $label;
  public $summary;
  public $direction;
  public $creation;
  public array $states = [];
  public string $home;// TODO the trunkward most node this workflow lives on.
  public $disabled;

  const RELATION_PAYEE = 'payee';
  const RELATION_PAYER = 'payer';
  const RELATION_AUTHOR = 'author';

  /**
   *
   * @param $info
   *   Note this may have come via json so make sure everything is cast correctly.
   */
  function __construct($info) {
    $info = (object)$info;
    $this->id = $info->id;
    $this->label = $info->label;
    $this->summary = $info->summary;
    // this is transitional as we active is deprecated in favour of disabled.
    $this->disabled = isset($info->disabled) ? $info->disabled : !$info->active;
    $this->direction = $info->direction;
    $this->creation = (object)$info->creation;
    $this->creation->confirm = (bool)$this->creation->confirm; // Not sure this is needed.
    // Converting these to arrays because php handles arrays better.
    foreach ((array)$info->states as $state => $data) {
      $data = (array)$data;
      foreach($data as &$dest_states) {
        $dest_states = (object)$dest_states;
      }
      $this->states[$state] = $data;
    }
    $this->home = $info->home??'';
  }

  /**
   * Get all the transitions the given user can do on the current transaction.
   *
   * @param string $acc_id
   * @param Transaction $transaction
   * @param bool $admin
   *   Admin can do all defined transitions
   * @return array
   */
  function getTransitions($acc_id, Transaction $transaction, bool $admin = FALSE): array {
    $links = [];
    if ($transaction->state == TransactionInterface::STATE_VALIDATED) {
#      if ($admin or $this->canTransitionToState($acc_id, $transaction->state, $transaction->entries[0], $this->creation->state)) {
      if ($this->canTransitionToState($transaction, $acc_id, $this->creation->state, $admin)) {
        $links[$this->creation->state] = '/transaction/'.$transaction->uuid.'/'.$this->creation->state;
        //signifies a cancel operation, or delete this unconfirmed transaction.
        $links['null'] = '/transaction/'.$transaction->uuid.'/null';
      }
    }
    elseif(isset($this->states[$transaction->state])) {
      $role = self::getAccRole($transaction, $acc_id);
      foreach ($this->states[$transaction->state] as $target_state => $info) {
        if ($admin or in_array($role, $info->actors)) {
          $links[$target_state] = "/transaction/$transaction->uuid/$target_state";
        }
      }
    }
    return $links;
  }

  /**
   * Check if the transition exists and if the user is permitted to do it, based
   * on the actors (payer & payee) defined for that transition.
   * @param Transaction $transaction
   * @param string $user_id
   * @param string $target_state
   * @param bool $check_permission
   *   Admin and remote nodes don't need to check permission.
   * @return bool
   */
  function canTransitionToState(Transaction $transaction, string $user_id, string $target_state, bool $check_permission = TRUE) :  bool {
    $current_state = $transaction->state;
    if ($current_state == $target_state) {
      throw new WorkflowViolation(from: $from_state, to: $target_state, type: $this->id);
    }
    if ($current_state == 'validated' and $target_state == $this->creation->state) {
      return TRUE;
    }
    if (!isset($this->states[$current_state][$target_state])) {
      throw new InvalidTransitionViolation(workflowId: $this->id, current_state: $current_state, target_state: $target_state);
    }
    if ($check_permission) {
      if ($role = self::getAccRole($transaction, $user_id)) {
        return in_array($role, $this->states[$current_state][$target_state]->actors);
      }
    }
    return TRUE;
  }

  /**
   * Determine the relationship(s) of the authenticated user to the current
   * transaction
   * @param $acc_id
   *   The name of the account, user or wallet, as it appears in the ledger
   * @param Entry $entry
   *   The main entry of a transaction
   * @return array
   *   A list of account names.
   *
   * @deprecated
   */
  function getMyRelations($acc_id, Entry $entry) : array {
    $relations = [];
    if($acc_id == $entry->payee->id) {// taken from the first entry.
      $relations[] = SELF::RELATION_PAYEE;
    }
    elseif ($acc_id == $entry->payer->id) {
      $relations[] = SELF::RELATION_PAYER;
    }
    if (isset($entry->author) and $acc_id == $entry->author) {// @todo this might be ambiguous.
      $relations[] = SELF::RELATION_AUTHOR;
    }
    // Todo test for blog relations on the other entries.
    return $relations;
  }

  /**
   * Determine if a user is payer, payee or neither in the main Entry of the transaction
   * @param Transaction $transaction
   * @param string $acc_id
   * @return string
   */
  static function getAccRole(Transaction $transaction, string $acc_id) : string {
    $role = ''; // Could be any string which will never be in 'actors'.
    if ($transaction->entries[0]->payee == $acc_id) {
      $role = 'payee';
    }
    elseif($transaction->entries[0]->payer == $acc_id) {
      $role = 'payer';
    }
    return $role;
  }

  /**
   * Get the labels of the transitions from the given state to the given states
   * @param string $current_state
   * @param array $allowed_states
   *   $paths, keyed by state_name
   * @return type
   */
  function actionLabels($current_state, array $allowed_states) : array {
    $labels = [];
    if ($current_state == TransactionInterface::STATE_VALIDATED) {
      $labels[$this->creation->state] = $this->creation->label;
      $labels['null'] = 'Cancel';
    }
    else {
      foreach ($allowed_states as $state => $path) {
        if (empty($this->states[$current_state][$state])) {
          // This should never happen.
          trigger_error("workflow $this->id doesn't allow transition from $current_state to $state");
          continue;
        }
        $labels[$state] = $this->states[$current_state][$state]->label;
      }
    }
    return $labels;
  }

  /**
   * Generate a thumbprint excluding labels (which may be localised).
   * @return string
   */
  function getHash() : string {
    $hashable_states=[];
    ksort($this->states);
    foreach ($this->states as $state => $target_states) {
      $target_states = (array)$target_states;
      ksort($target_states);
      foreach ($target_states as $target_name => $target_state) {
        $hashable_states[$state][$target_name] = $target_state->actor;
      }
    }
    $creation = clone($this->creation);
    unset($creation->label);
    $string = json_encode($creation) .$this->direction . json_encode($hashable_states);
    return md5($string);
  }
}
