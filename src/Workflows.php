<?php
namespace CreditCommons;

use CreditCommons\NodeRequester;
use CreditCommons\Workflow;
use CreditCommons\Exceptions\DoesNotExistViolation;
use CreditCommons\Exceptions\DeprecatedWorkflowViolation;

/**
 * Class for clients and base class for nodes. Override this class to incorporate
 * locally defined workflows.
 */
class Workflows implements \JsonSerializable {

  private $trunkwardNodeRequester;

  /**
   * All workflows, localised.
   * @todo this should be stored, not regenerated from the trunkwards nodes on every request.
   * @var array
   */
  public $all = [];

  function __construct(NodeRequester $trunkward_node = NULL) {
    // N.B. nodes might not have a trunkward node.
    $this->trunkwardNodeRequester = $trunkward_node;
    $this->all = $this->loadAll();
  }

  /**
   * This function seems is a mere alias
   */
  function loadAll() {
    static $data = [];
    if (empty($data) and $this->trunkwardNodeRequester) {
      $data = $this->trunkwardNodeRequester->getWorkflows();
    }
    return $data;
  }

  /**
   * @param type $needed_id
   * @param array $all_workflows
   * @return Workflow
   * @throws DoesNotExistViolation
   */
  function get($needed_id) : Workflow {
    foreach ($this->all as $workflow) {
      if ($workflow->id == $needed_id) {
        if ($workflow->disabled) {
          throw new DeprecatedWorkflowViolation(id: $workflow->id);
        }
        return $workflow;
      }
    }
    throw new DoesNotExistViolation(type: 'workflow', id: $needed_id);
  }

  /**
   * Get a list of workflows available for making transactions.
   * @return array
   */
  function getActiveWorkflowIds() : array {
    foreach ($this->all as $wfs) {
      foreach ($wfs as $wf) {
        if (empty($wf->disabled)) {
          $ids[] = $wf->id;
        }
      }
    }
  }

  public function jsonSerialize(): mixed {
    return $this->all;
  }

}

