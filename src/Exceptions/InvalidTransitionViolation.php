<?php

namespace CreditCommons\Exceptions;

/**
 * The workflow denies permission to the user to perform the action on the transaction.
 */
class InvalidTransitionViolation extends CCViolation {

  public function __construct(
    // The Wirkflow ID
    public string $workflowId,
    public string $current_state,
    // The state to which the transaction may not be moved
    public string $target_state
  ) {
    parent::__construct();

  }

  function makeMessage() : string {
    return "The $this->workflowId' workflow does not define a transition from state '$this->current_state' to '$this->target_state'.";
  }
}
