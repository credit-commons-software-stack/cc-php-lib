<?php

namespace CreditCommons\Exceptions;

/**
 * The workflow denies permission to the user to perform the action on the transaction.
 */
class DuplicateUuidViolation extends CCViolation {

  public function __construct(
    // The Workflow ID
    public string $uuid
  ) {
    parent::__construct();
  }

  function makeMessage() : string {
    return "A transaction already exists with uuid $this->uuid.";
  }
}
