<?php

namespace CreditCommons\Exceptions;

/**
 * Violation for when the hashes of two ledgers don't match.
 */
final class HashMismatchFailure extends CCFailure {

  public function __construct(
    public string $remoteNodeName,
    // The hash on the local node.
    public string $localHash,
    public string $remoteHash,
    public string $translated = 'blah'
  ) {
    parent::__construct(translated: 'The hashes do not match.');
  }

  function makeMessage(): string  {
    return "The hash from node $this->remoteNodeName ($this->remoteHash) does not match the local hash ($this->localHash) on $this->node.";
  }

}
