<?php

namespace CreditCommons\Exceptions;

/**
 * Pass $acc_id, $diff
 */
class MaxLimitViolation extends TransactionLimitViolation {

  /**
   * {@inheritDoc}
   */
  function makeMessage() : string {
    $acc = $this->acc == '*' ? 'Balance of Trade' : $this->acc;
    return "This transaction would put account '$acc' on $this->node above its maximum balance by '$this->diff'";
  }
}
