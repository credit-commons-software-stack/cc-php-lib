<?php

namespace CreditCommons;
use CCNode\Accounts\Trunkward;
use CreditCommons\AccountRemoteInterface;

trait TransversalTransactionTrait {

  /**
   * Filter the entries for those that pertain to a certain node.
   * Make a clone of the transaction with only the entries shared with an
   * adjacent ledger.
   *
   * @param string $remote_acc_id
   */
  protected function filterFor(string $remote_acc_id) : array {
    // Filter entries for the appropriate adjacent ledger
    foreach ($this->entries as $e) {
      if ($e->payee->id == $remote_acc_id or $e->payer->id == $remote_acc_id) {
        $entries[] = $e;
      }
    }
    return $entries;
  }

  /**
   * Generate the hash for a Remote account.
   *
   * @param AccountRemoteInterface $account
   * @return string
   */
  function makeHash(AccountRemoteInterface $account) : string {
    $string = $this->makeHashString($account);
    return md5($string);
  }

  /**
   * @param AccountRemoteInterface $account
   * @return string
   *
   * @todo Establish the entry->description charset in case there's a risk of
   * different servers hashing it differently
   * @todo why doesn't this hash include the transaction type?
   */
  private function makeHashString(AccountRemoteInterface $account) : string {
    $trunkward = $account instanceOf Trunkward;
    $rows = [];
    foreach ($this->filterFor($account->id) as $entry) {
      $quant = $trunkward ? $entry->trunkwardQuant : $entry->quant;
      // Storing the trunkward quant is a way of avoiding rounding errors.
      $rows[] = $quant.'|'.$entry->description;
      // todo it could also store the trunkward accountnames.
    }
    return join('|', [
      $account->getLastHash(),
      $this->uuid,
      $this->state,
      join('|', $rows),
      $this->version,
    ]);
  }

}
