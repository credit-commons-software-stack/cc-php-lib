<?php

namespace CreditCommons\Leaf;

use CreditCommons\BaseTransaction;
use CreditCommons\TransversalTransactionTrait;

/**
 * Transaction for client side display including transitions property.
 */
class TransversalTransaction extends BaseTransaction {
  use TransversalTransactionTrait;

  static function createFromNew(\CreditCommons\NewTransaction $new) {
    $data = new \stdClass;
    $data->uuid = $new->uuid;
    $data->type = $new->type;
    $data->state = TransactionInterface::STATE_INITIATED;
    $data->version = -1; // Corresponds to state init, pre-validated.
    $data->entries = [(object)[
      'payee' => $new->payee,
      'payer' => $new->payer,
      'description' => $new->description,
      'metadata' => $new->metadata,
      'quant' => $new->quant,
      'isPrimary' => TRUE
    ]];
    return new static($data->uuid, $data->type, $data->state, $data->entries, $data->written??'', $data->version);
  }


  function buildValidate() {
    // Add any fees
    // Check balance limits.
  }

}
