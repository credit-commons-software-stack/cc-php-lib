<?php

namespace CreditCommons;
use CreditCommons\EntryFlat;

/**
 * Just declare the properties of the transaction, according to what is
 * delivered back to the client from POST /transaction or GET /transactions
 */
class BaseTransaction  {
  use CreateFromValidatedStdClassTrait;

  public array $transitions;
  private $mainTemplate = "<p align=center>On @date @payer paid @payee<br />@amount<br />for<br />@description.</p>";
  private $feeTemplate = "<font size=-1>@payer paid @payee @amount for @description</font>";

  public function __construct(
    public string $uuid,
    public string $type,
    public string $state,
    /** @var Entry[] $entries */
    public array $entries,
    public string $written,
    public int $version
  ) { }

  /**
   * @param \stdClass $data
   * @return static
   */
  static function create(\stdClass $data) : static {
    $data->version = $data->version??-1;
    $data->written = $data->written??'';
    $data->state = $data->state??TransactionInterface::STATE_INITIATED;
    static::validateFields($data);
    return new static($data->uuid, $data->type, $data->state, $data->entries, $data->written, $data->version);
  }

  /**
   * {@inheritDoc}
   */
  public static function createFromJsonClass(\stdClass $data, array $transitions) : static {
    if (get_called_class() == get_class()) {
      throw new CCFailure('Cannot call base transaction class directly.');
    }
    foreach ($data->entries as &$entry) {
      if (!isset($entry->metadata)) {
        $entry->metadata = new \stdClass;
      }
      $entry = EntryFlat::create($entry);
    }

    $t = static::create($data);
    $t->transitions = $transitions;
    return $t;
  }

  // A json object to pass back to the leaf
  function JsonDisplayable() : array {
    return [
      'uuid' => $this->uuid,
      'written' => $this->written,
      'state' => $this->state,
      'type' => $this->type,
      'version' => $this->version,
      'entries' => $this->entries
    ];
  }


  /**
   * Render the transaction action links as forms which can post. (Client side only)
   *
   * @param array $transitions
   * @return string
   */
  public function actionLinks(array $transitions) : string {
    if ($transitions) {
      $output[] = '<form method="post" class="inline" action="">';
      $output[] = '<input type="hidden" name="uuid" value="'.$this->uuid.'">';
      foreach ($this->getWorkflow()->actionLabels($this->state, $transitions) as $target_state => $label) {
        $output[] = '<button type="submit" name="stateChange" value="'.$target_state.'" class="link-button">'.$label.'</button>';
      }
      $output[] = '</form>';
    }
    else {
      $output[] = "<span title = \"Current user is not permittions to do anything to this '$this->type' transaction\">(no actions)</span>";
    }
    return implode($output);
  }

  /**
   * Make a basic HTML render of the transaction.
   */
  public function render() :string {
    $main_entry = array_shift($this->entries);
    $replacements = [
      '@date' => $this->written,
      '@amount' => $main_entry->quant,
      '@payer' => $main_entry->payer,
      '@payee'=> $main_entry->payee,
      '@description' => $main_entry->description,
    ];
    $output = strtr($this->mainTemplate, $replacements);
    if ($this->entries) {
      foreach ($this->entries as $entry) {
        $replacements = [
          '@amount' => $entry->quant,
          '@payer' => $entry->payer,
          '@payee'=> $entry->payee,
          '@description' => $entry->description,
        ];
        $additional[] = strtr($this->feeTemplate, $replacements);
      }
      $output .= '<p align="center">'.implode('<br >', $additional).'</p>';
    }
    return $output;
  }

}
