<?php

namespace CreditCommons;

use CreditCommons\BaseTransaction;
use CreditCommons\TransactionInterface;
use CreditCommons\Workflow;

/**
 * A transaction consists of several ledger entries sharing a common UUID, type and workflow state.
 * There is a primary entry, and many dependents.
 * Entries can be either transversal or local.
 * The transaction is either transversal or local, depending on the primary entry.
 * That means a local transaction cannot have transversal dependents.
 */
class Transaction extends BaseTransaction implements TransactionInterface {


  /**
   * Retrieve the whole workflow object, determined by the transaction->type.
   *
   * Can't implement this in cc-php-lib because workflow storage and loading
   * into config is specific to the application. Nonetheless the function is here
   * because the class be used by the client who has no need of workflows.
   * So it's fine as long as this is only called when extended.
   *
   * @return Workflow
   */
  public function getWorkflow() : Workflow {}

}
